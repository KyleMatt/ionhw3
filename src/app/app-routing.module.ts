import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';


const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: '',
  //   pathMatch: 'full',
  // },
  {
    path: '', component: MainComponent,
    children: [
      {
        path: 'customers',
        loadChildren: () =>
          import('./Pages/customers/customers.module').then(
            (m) => m.CustomersPageModule
          ),
      },
      {
        path: 'product-type',
    loadChildren: () =>
      import('./Pages/product-type/product-type.module').then(
        (m) => m.ProductTypePageModule
      ),
      },
      {
        path: 'products',
        loadChildren: () =>
          import('./Pages/products/products.module').then(
            (m) => m.ProductsPageModule
          ),
      },
      {
        path: 'sales-employee',
        loadChildren: () =>
          import('./Pages/sales-employee/sales-employee.module').then(
            (m) => m.SalesEmployeePageModule
          ),
      },
      {
        path: 'employee-type',
        loadChildren: () =>
          import('./Pages/employee-type/employee-type.module').then(
            (m) => m.EmployeeTypePageModule
          ),
      }
    ],
  }]

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
